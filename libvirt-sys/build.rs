use std::error::Error;
use std::path::{Path, PathBuf};
use std::{env, process};

fn main() {
    match run() {
        Ok(()) => {}
        Err(e) => {
            eprintln!("error: {}", e);
            process::exit(1);
        }
    }
}

fn run() -> Result<(), Box<dyn Error>> {
    let deps = system_deps::Config::new().probe()?;
    let lib = deps.get_by_name("virt").unwrap();

    let header_dir = lib
        .include_paths
        .iter()
        .find_map(|p| find_libvirt_header_dir(p))
        .ok_or_else(|| String::from("could not find libvirt header directory"))?;

    let bindings = bindgen::builder()
        .header(header_dir.join("libvirt.h").to_string_lossy())
        .header(header_dir.join("virterror.h").to_string_lossy())
        .allowlist_var("^(VIR_|vir).*")
        .allowlist_type("^vir.*")
        .allowlist_function("^vir.*")
        .generate_comments(false)
        .prepend_enum_name(false)
        .generate()
        .map_err(|_| String::from("could not generate bindings"))?;

    let out_dir = PathBuf::from(env::var("OUT_DIR")?);
    bindings.write_to_file(out_dir.join("bindings.rs"))?;

    Ok(())
}

fn find_libvirt_header_dir(include_path: &Path) -> Option<PathBuf> {
    let header_dir = include_path.join("libvirt");
    if header_dir.exists() {
        Some(header_dir)
    } else {
        None
    }
}
