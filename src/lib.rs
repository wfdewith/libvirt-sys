extern crate libvirt_sys as ffi;

pub fn some_function_already_in_v6_0_0() {
}

#[cfg(feature = "v7_0_0")]
pub fn some_function_added_in_v7_0_0() {
}

#[cfg(feature = "v8_0_0")]
pub fn some_function_added_in_v8_0_0() {
}
